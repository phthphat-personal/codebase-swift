import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(DatabaseLayerTests.allTests),
    ]
}
#endif
