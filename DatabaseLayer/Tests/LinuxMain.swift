import XCTest

import DatabaseLayerTests

var tests = [XCTestCaseEntry]()
tests += DatabaseLayerTests.allTests()
XCTMain(tests)
