//
//  Networking.swift
//  ComplicatedGroupChat
//
//  Created by Lucas Pham on 12/15/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import Foundation
import PNetworking
import RxSwift

//public typealias HandleApiRes<Model: Codable> = (Result<Model?, APIError>) -> Void
//public typealias HandlePagingApiRes<Model: Codable> = (Result<(Model?, MetaData?), APIError>) -> Void
//public typealias HandleNetworkRes = (Result<Data, Error>) -> Void

public protocol APIService {
    var networking: PNetworking { get }
    init(networking: PNetworking)
}

extension APIService {
    public func request<Model: Codable>(_ route: EndPoint) -> Observable<Model> {
        return .create { observer -> Disposable in
            self.networking.request(endPoint: route) { res in
                switch res {
                case .success(let data):
                    if let apiError = PParser<APIError>(data: data).toObject() {
                        if let code = apiError.code, code > 200 && code <= 300 {
                            observer.onError(
                                NSError(domain: "", code: code, userInfo: [NSLocalizedDescriptionKey: apiError.message ?? ""])
                            )
                        }
                    }

                    if let apiRes = PParser<APIResponse<Model>>(data: data).toObject(), let model = apiRes.data {
                        observer.onNext(model)
                    }
                case .failure(let err):
                    observer.onError(err)
                }
            }
            return Disposables.create()
        }
    }
    public func requestPaging<Model: Codable>(_ route: EndPoint) -> Observable<(Model, MetaData)> {
        return .create { observer -> Disposable in
            self.networking.request(endPoint: route) { res in
                switch res {
                case .success(let data):
                    if let apiError = PParser<APIError>(data: data).toObject() {
                        if let code = apiError.code, code > 200 && code <= 300 {
                            observer.onError(
                                NSError(domain: "", code: code, userInfo: [NSLocalizedDescriptionKey: apiError.message ?? ""])
                            )
                        }
                    }

                    if let apiRes = PParser<APIResponse<Model>>(data: data).toObject(), let model = apiRes.data, let metaData = apiRes.metaData {
                        observer.onNext((model, metaData))
                    }
                case .failure(let err):
                    observer.onError(err)
                }
            }
            return Disposables.create()
        }
    }
    public func requestForm<Model: Codable>(_ route: EndPoint) -> Observable<Model> {
        return .create { observer -> Disposable in
            self.networking.requestFormData(endPoint: route, handle: { res in
                switch res {
                case .success(let data):
                    if let apiError = PParser<APIError>(data: data).toObject() {
                        if let code = apiError.code, code > 200 && code <= 300 {
                            observer.onError(
                                NSError(domain: "", code: code, userInfo: [NSLocalizedDescriptionKey: apiError.message ?? ""])
                            )
                        }
                    }

                    if let apiRes = PParser<APIResponse<Model>>(data: data).toObject(), let model = apiRes.data {
                        observer.onNext(model)
                    }
                case .failure(let err):
                    observer.onError(err)
                }
            })
            return Disposables.create()
        }
    }
}

//extension APIService {
//
//    public func request<Model: Codable>(route: EndPoint, handler: @escaping HandleApiRes<Model>) {
//        networking.request(endPoint: route, handle: handleApiResonse(handler))
//    }
//
//    public func requestPaging<Model: Codable>(route: EndPoint, handler: @escaping HandlePagingApiRes<Model>) {
//        networking.request(endPoint: route, handle: handlePagingApiResonse(handler))
//    }
//
//    public func requestFormData<Model: Codable>(route: EndPoint, handler: @escaping HandleApiRes<Model>) {
//        networking.requestFormData(endPoint: route, handle: handleApiResonse(handler))
//    }
//
//    private func handleApiResonse<Model: Codable>(_ handleApi: @escaping HandleApiRes<Model>) -> HandleNetworkRes {
//        return { result in
//            switch result {
//            case .success(let data):
//                if let apiError = PParser<APIError>(data: data).toObject() {
//                    if let code = apiError.code, code > 200 && code <= 300 {
//                        handleApi(.failure(apiError))
//                    }
//                }
//                if let apiRes = PParser<APIResponse<Model>>(data: data).toObject() {
//                    handleApi(.success(apiRes.data))
//                }
//            case .failure(let err):
//                handleApi(.failure(APIError(error: err)))
//            }
//        }
//    }
//    private func handlePagingApiResonse<Model: Codable>(_ handleApi: @escaping HandlePagingApiRes<Model>) -> HandleNetworkRes {
//        return { result in
//            switch result {
//            case .success(let data):
//                if let apiError = PParser<APIError>(data: data).toObject() {
//                    if let code = apiError.code, code > 200 && code <= 300 {
//                        handleApi(.failure(apiError))
//                    }
//                }
//                if let apiRes = PParser<APIResponse<Model>>(data: data).toObject() {
//                    handleApi(.success((apiRes.data, apiRes.metaData)))
//                }
//            case .failure(let err):
//                handleApi(.failure(APIError(error: err)))
//            }
//        }
//    }
//}



enum APIResponseType {
    case object, paging(page: Int), formData
}
