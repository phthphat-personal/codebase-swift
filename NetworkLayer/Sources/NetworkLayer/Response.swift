//
//  NetworkRequest.swift
//  BaseProject
//
//  Created by Lucas Pham on 12/17/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import Foundation

public class APIResponse<Model: Codable>: Codable {
    public var code: Int?
    public var metaData: MetaData?
    public var data: Model?
    public var message: String?
    
    private enum CodingKeys: String, CodingKey {
        case code, metaData = "metadata", data, message
    }
}

public class MetaData: Codable {
    var currentPage: Int?
    var totalPages: Int?
    
    private enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case totalPages = "total_pages"
    }
}

public class APIError: Error, Codable {
    public var code: Int?
    public var message: String?
    
    init(error: Error) {
        self.message = error.localizedDescription
    }
}
