//
//  BaseCltV.swift
//  Omnitest
//
//  Created by Lucas Pham on 1/3/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
open class BaseCltV<Cell: BaseCltVCell<Model>,Model: Codable>: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate {
    

    open var items: [Model] = []
    public var cellId = ""
    
    //Closure
    public var didTapOnModel: (Model) -> Void = { _ in }
    public var needLoadMore: () -> Void = {  }
    public var needRefresh: () -> Void = {  }

//    scrollDirection() -> UICollectionViewFlowLayout
    
    override public init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        preInitView()
        setUp()
    }
    convenience public init(frame: CGRect) {
        self.init(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())
    }
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        
        preInitView()
        setUp()
    }
    //Override to use
    open func setUp() {
        
    }
    
    //Private
    private func preInitView() {
        backgroundColor = .white
        
//        var layOut = UICollectionViewFlowLayout()
//        layOut.scrollDirection = .vertical
        
        self.dataSource = self
        self.delegate = self
        cellId = String(describing: Cell.self)
        self.register(Cell.self, forCellWithReuseIdentifier: cellId)
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    @objc private func refresh() {
        needRefresh()
    }
    
    //Datasource and delegate
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Num item: ", items.count)
        return items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? Cell else {
            return UICollectionViewCell()
        }
        let item = items[indexPath.row]
        cell.setUpCell(item: item)
        return cell
    }
}
