//
//  BaseTbV.swift
//  BaseProject
//
//  Created by Phthphat on 12/12/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
open class BaseTbV<Model, Cell: BaseTbVCell<Model>>: UITableView, UITableViewDataSource, UITableViewDelegate {
    
    open var items: [Model] = []
    open var cellId: String = "CellId"
    open var curPage: Int = 0
    
    //Closure
    var didTapOnModel: (Model) -> Void = { _ in }
    var needLoadMore: () -> Void = {  }
    var needRefresh: () -> Void = {  }
    
    
    override public init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        preInitView()
        setUp()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        
        preInitView()
        setUp()
    }
    private func preInitView() {
        self.dataSource = self
        self.delegate = self
        cellId = String(describing: Cell.self)
        self.register(Cell.self, forCellReuseIdentifier: cellId)
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    open func setUp() {}
    
    public func setData(items: [Model], isLoadMore: Bool = false) {
        self.items = isLoadMore ? self.items + items : items
        self.reloadData()
        if !isLoadMore {
            self.refreshControl?.endRefreshing()
        }
    }
    
    @objc private func refresh() {
        needRefresh()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? Cell {
            let item = items[indexPath.row]
            cell.item = item
            return cell
        }
        return UITableViewCell()
    }
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == items.count - 1 {
            curPage += 1
            needLoadMore()
        }
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.items[indexPath.row]
        didTapOnModel(model)
    }
}
