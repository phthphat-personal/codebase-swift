//
//  BaseCltVCell.swift
//  Omnitest
//
//  Created by Lucas Pham on 1/3/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit

open class BaseCltVCell<Model: Codable>: UICollectionViewCell {
    open var item: Model? {
        didSet {
            guard let item = item else { return }
            self.setUpCell(item: item)
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        //Default setup
        
        setUp()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //Override to use
    open func setUp() {}
    open func setUpCell(item: Model) {}
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//    }
}
