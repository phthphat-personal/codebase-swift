//
//  BaseTbVC.swift
//  BaseProject
//
//  Created by phthphat on 12/12/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import UIKit

open class BaseTbVCell<Model>: UITableViewCell {
    var item: Model? {
        didSet {
            guard let item = item else { return }
            self.setUpCell(item: item)
        }
    }
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //Default setup
        selectionStyle = .none
        
        setUp()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setUp() {}
    
    open func setUpCell(item: Model) {}
    
    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
