//
//  BaseView.swift
//  Omnitest
//
//  Created by Lucas Pham on 12/7/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import UIKit

open class BaseV: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    open func setUp() {}
}
