//
//  BaseTbVC.swift
//  Omnitest
//
//  Created by Lucas Pham on 12/23/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import UIKit

open class Base1SectionTbVC<Cell: BaseTbVCell<Model>, Model>: UITableViewController, BaseVCHelper {
    
    open var items: [Model] = []
    open var cellId = String(describing: Cell.self)
    fileprivate var canPull2Refresh: Bool = true
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        preSetUp()
        setUp()
    }
    fileprivate func preSetUp() {
        self.view.backgroundColor = .white
        self.tableView = .init(frame: .zero, style: .grouped)
        self.tableView.separatorStyle = .none
        tableView.register(Cell.self, forCellReuseIdentifier: cellId)
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    // MARK: - Override to use
    open func setUp(){}
    open func loadMore(){}
    @objc open func refresh() { self.endRefresh() }
    
    // MARK: - Available to use
    public func addHeader(_ view: UIView, height: CGFloat) {
        view.frame = .init(x: 0, y: 0, width: self.view.bounds.width, height: height)
        tableView.tableHeaderView = view
    }
    public func addFooter(_ view: UIView, height: CGFloat) {
        view.frame = .init(x: 0, y: 0, width: self.view.bounds.width, height: height)
        tableView.tableFooterView = view
    }
    public func makePull2Refresh(isActive: Bool) {
        self.canPull2Refresh = isActive
        if !isActive {
            self.refreshControl = nil
        }
    }
    private func endRefresh() {
        self.refreshControl?.endRefreshing()
    }
    
    // MARK: - Table view data source

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.items.count - 1 {
            loadMore()
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? Cell else {
            return UITableViewCell()
        }
        cell.item = items[indexPath.row]
        return cell
    }
    
    public enum TitleViewStyle {
        case full
        case custom(width: CGFloat)
    }
    public enum BarButtonSide {
        case left
        case right
    }
}
