//
//  BaseNavController.swift
//  Omnitest
//
//  Created by Lucas Pham on 1/3/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit

open class BaseNavController: UINavigationController, UIGestureRecognizerDelegate {

    override public init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        navigationBar.barTintColor = self.setBarTintColor()
        self.interactivePopGestureRecognizer?.isEnabled = true
        self.interactivePopGestureRecognizer?.delegate = self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //Override to use
    open func setBarTintColor() -> UIColor { return .white }
    open func setUp() {}
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.interactivePopGestureRecognizer && self.viewControllers.count > 1 {
            return true
        }
        return false
    }
}
