//
//  BaseVC.swift
//  Omnitest
//
//  Created by Lucas Pham on 12/7/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import UIKit

//MARK: Navigation bar
public protocol BaseVCHelper: UIViewController {
}

extension BaseVCHelper {
    public var leftBarBtn: UIBarButtonItem? {
        get {
            return self.navigationItem.leftBarButtonItem
        }
        set {
            return self.navigationItem.leftBarButtonItem = newValue
        }
    }
    
    public var rightBarBtn: UIBarButtonItem? {
        get {
            return self.navigationItem.rightBarButtonItem
        }
        set {
            return self.navigationItem.rightBarButtonItem = newValue
        }
    }
    
    public func setTitle(_ string: String) {
        self.title = string
    }
    
    public func addTitleView(_ view: UIView, style: TitleViewStyle = .custom(width: 50)) {
        var widthTitleV: CGFloat = 50
        switch style {
        case .custom(let width):
            widthTitleV = width
        case .full:
            widthTitleV = UIScreen.main.bounds.width
        }
        view.frame = .init(x: 0, y: 0, width: widthTitleV, height: 44)
        view.translatesAutoresizingMaskIntoConstraints = true
        self.navigationItem.titleView = view
    }
    public func setNavigationBar(hide: Bool) {
        self.navigationController?.navigationBar.isHidden = hide
    }
    public func addBarButton(image: UIImage, selector: Selector, side: BarButtonSide = .left) {
        let barButton = UIBarButtonItem(image: image, style: .plain, target: self, action: selector)
        switch side {
        case .left:
            self.leftBarBtn = barButton
        case .right:
            self.rightBarBtn = barButton
        }
    }
    public func addBarButton(title: String, selector: Selector, side: BarButtonSide = .left) {
        let barButton = UIBarButtonItem(title: title, style: .done, target: self, action: selector)
        barButton.tintColor = .black
        switch side {
        case .left:
            self.leftBarBtn = barButton
        case .right:
            self.rightBarBtn = barButton
        }
    }
}

public enum TitleViewStyle {
    case full
    case custom(width: CGFloat)
}
public enum BarButtonSide {
    case left
    case right
}

//MARK: Appearance
extension BaseVCHelper {
    public func setBackground(with color: UIColor) {
        self.view.backgroundColor = color
    }
}

//MARK: Navigation
extension BaseVCHelper {
    public func push(_ viewController: UIViewController, animated: Bool = true) {
        self.navigationController?.pushViewController(viewController, animated: animated)
    }
}

//MARK: Concurrency, Multi-threading
extension BaseVCHelper {
    public var mainQueue: DispatchQueue {
        return DispatchQueue.main
    }
    public var backgroundQueue: DispatchQueue {
        return DispatchQueue.global(qos: .background)
    }
    public var utilityQueue: DispatchQueue {
        return DispatchQueue.global(qos: .utility)
    }
    public func mainAsync(execute: @escaping () -> Void) {
        mainQueue.async {
            execute()
        }
    }
    public func backgroundAsync(execute: @escaping () -> Void) {
        backgroundQueue.async {
            execute()
        }
    }
    public func utilityAsync(execute: @escaping () -> Void) {
        utilityQueue.async {
            execute()
        }
    }
}
