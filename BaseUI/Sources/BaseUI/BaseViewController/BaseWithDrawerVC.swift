//
//  BaseWithDrawerVC.swift
//  DrawerBase
//
//  Created by Lucas Pham on 12/31/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import UIKit

@available(iOS 9.0, *)
open class BaseWithDrawerVC: UIViewController, BaseVCHelper, HasDrawer, DrawerDelegate {
    
    public var drawerBtn: UIBarButtonItem?
    private var appView: UIWindow?
    private var fullView = UIView()
    private var darkView = UIView()
    private var leadingAnchor: NSLayoutConstraint?
    private var widthAnchor: NSLayoutConstraint?
    private var maxPanSize: CGFloat!
    private var hasSetUpView = false
    private var hamburgerWidth: CGFloat {
        let baseRatio: CGFloat = 5/6
        let baseWidth: CGFloat = 400
        guard let frame = UIApplication.shared.windows.last?.frame else { return baseWidth }
        return frame.width * baseRatio < baseWidth ? frame.width * baseRatio : baseWidth
    }
    public var isClosed = true
    public var hamburgerView: UIView?
    
    //Override to use
    open func viewForDrawer() -> UIView {
        let v = UIView()
        v.backgroundColor = .systemGreen
        return v
    }
    open func titleForDrawerBtn() -> String? { return nil }
    open func imageForDrawerBtn() -> UIImage? { return nil }
    open func colorForShadow() -> UIColor { return UIColor.black.withAlphaComponent(0.2) }
    open func drawer(buttonClick button: UIBarButtonItem?) {}
    open func drawer(didDismiss button: UIBarButtonItem?) {}
    
    //
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        hamburgerView = viewForDrawer()
        
        if let title = titleForDrawerBtn() {
            drawerBtn = .init(title: title, style: .plain, target: self, action: #selector(buttonPress))
        } else if let image = imageForDrawerBtn() {
            drawerBtn = .init(image: image, style: .plain, target: self, action: #selector(buttonPress))
        } else {
            drawerBtn = .init(title: "Show", style: .plain, target: self, action: #selector(buttonPress))
        }
//        drawerBtn = .init(title: "huhu", style: .plain, target: self, action: #selector(buttonPress))
        self.navigationItem.leftBarButtonItem = drawerBtn
        setUpView()
        addGesture()
    }

    private func setUpView(){
        if self.hasSetUpView { return }
        if let frame = UIApplication.shared.windows.last?.frame {
            fullView.backgroundColor = .clear
            fullView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            fullView.isOpaque = false
            var _window: UIWindow?
            if #available(iOS 13.0, *) {
                _window = UIApplication.shared.connectedScenes
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
                
            } else {
                guard let _tempWindow = UIApplication.shared.delegate?.window else { return }
                _window = _tempWindow
            }
            darkView.backgroundColor = colorForShadow()
            self.appView = _window
            guard let appView = self.appView else { return }
            hamburgerView?.frame = CGRect(x: 0, y: 0, width: hamburgerWidth, height: frame.height)
            fullView.addSubview(hamburgerView ?? UIView())
            appView.addSubview(fullView)
            fullView.isUserInteractionEnabled = true
            fullView.translatesAutoresizingMaskIntoConstraints = false
            widthAnchor = fullView.widthAnchor.constraint(equalToConstant: frame.width)
            widthAnchor?.isActive = true
            leadingAnchor = fullView.leadingAnchor.constraint(equalTo: appView.leadingAnchor, constant: -appView.frame.width)
            leadingAnchor?.isActive = true
            fullView.topAnchor.constraint(equalTo: appView.topAnchor, constant: 0).isActive = true
            fullView.bottomAnchor.constraint(equalTo: appView.bottomAnchor, constant: 0).isActive = true
            
            fullView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(fullViewTapped(_:))))
            fullView.isHidden = true
            self.hasSetUpView = true
        }
    }
    private func addGesture(){
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panInSelf(_:)))
        self.fullView.addGestureRecognizer(panGesture)
    }
    
    public func updateWhenDeviceRotate(size: CGSize) { //Need to improve later
        widthAnchor?.constant = size.width
    }
    private var initialCenter = CGPoint()
    @objc private func panInSelf(_ gesture: UIPanGestureRecognizer){
        guard let hamburgerView = self.hamburgerView else { return }
        maxPanSize = (fullView.frame.width) / 2
        guard gesture.view != nil else {return}
        guard let piece = gesture.view else { return }
        // Get the changes in the X and Y directions relative to
        // the superview's coordinate space.
        let translation = gesture.translation(in: piece.superview)
        let velocity = gesture.velocity(in: piece.superview)
        if gesture.state == .began {
            self.initialCenter = piece.center
        }
        // Update the position for .changed
        if gesture.state == .changed {
            if translation.x > 0  {
                return
            }
            // Add the X and Y translation to the view's original position.
            let newCenter = CGPoint(x: initialCenter.x + translation.x, y: initialCenter.y)
            UIView.animate(withDuration: 0.1) {
                piece.center = newCenter
            }
        } else if gesture.state == .ended {
            // Add the X and Y translation to the view's original position.
            if -translation.x > hamburgerView.frame.width / 1.5 || -velocity.x > 300 {
                hideDrawer()
            } else {
                UIView.animate(withDuration: 0.1) {
                    piece.center = self.initialCenter
                }
            }
        }
    }
    
    private func makeShadow(view: UIView, add: Bool = true){
        //make shadow
        view.layer.masksToBounds = false
        let shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: view.frame.width + 10, height: view.frame.height), cornerRadius: 1)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowPath = shadowPath.cgPath
    }
    private func makeAppViewDarker(on: Bool) {
        //make darker
        if !on {
            darkView.removeFromSuperview()
            return
        }
        appView?.insertSubview(darkView, belowSubview: fullView)
        darkView.frame = UIScreen.main.bounds
    }
    
    public func hideDrawer(){
        guard let appView = self.appView else { return }
        makeAppViewDarker(on: false)
        fullView.isHidden = false
        UIView.animate(withDuration: 0.2, animations: {
            self.leadingAnchor?.constant = -appView.frame.width
            self.appView?.layoutIfNeeded()
        }) { (_) in
//            self.delegate?.dismissSlider?()
            self.drawer(didDismiss: self.drawerBtn)
            self.fullView.isHidden = true
        }
        self.isClosed = true
    }
    public func showDrawer() {
        self.fullView.isHidden = false
        makeAppViewDarker(on: true)
        UIView.animate(withDuration: 0.2, animations: {
            self.leadingAnchor?.constant = 0
            self.appView?.layoutIfNeeded()
        })
        isClosed = false
    }
    
    @objc private func fullViewTapped(_ sender: UITapGestureRecognizer){
        let location = sender.location(in: fullView)
        guard let hamburgerView = self.hamburgerView else { return }
        if !hamburgerView.frame.contains(location) {
            hideDrawer()
        }
    }
    
    @objc private func buttonPress(){
        self.drawer(buttonClick: self.drawerBtn)
        guard let appView = self.appView else { return }
        widthAnchor?.constant = appView.frame.width
        showDrawer()
    }
}

@objc protocol DrawerDelegate: class {
    @objc optional func drawer(buttonClick button: UIBarButtonItem?)
    @objc optional func drawer(didDismiss button: UIBarButtonItem?)
}
