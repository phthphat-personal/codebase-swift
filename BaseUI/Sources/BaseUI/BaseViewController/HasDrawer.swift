//
//  Drawerable.swift
//  DrawerBase
//
//  Created by Lucas Pham on 12/31/19.
//  Copyright © 2019 phthphat. All rights reserved.
//

import Foundation

@objc protocol HasDrawer {
    @objc optional func tappedOnText(_ text: String)
    @objc func showDrawer()
    @objc func hideDrawer()
}
