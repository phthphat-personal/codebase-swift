# Codebase - Swift
My personal swift codebase

## Contain
- Network layer
- Example project
- Core data helper

## Design Pattern
- MVVM software architecture
- Dependency injection
- Avoid or minimize using singleton

## Dependency Package
- My [P* packages](https://gitlab.com/phthphat-share) family (PNetworking, Extensions, PopUp)
- `RxSwift` (will be replaced by `Combine` in future)

## Contributor
- [Phat Pham](https://gitlab.com/phthphat)