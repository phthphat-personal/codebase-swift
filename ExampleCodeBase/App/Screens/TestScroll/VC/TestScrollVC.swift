//
//  TestScrollVC.swift
//  ExampleCodeBase
//
//  Created by Lucas Pham on 2/24/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit

class TestScrollVC: AppVC, UIScrollViewDelegate {

    var y: CGFloat = -88
    var lastY: CGFloat = 0
    
    let scrollV = UIScrollView()
    
    override func setUp() {
        self.view.addSubview(scrollV)
        scrollV.fillSuperview()
            
        let mainView = UIView()
        scrollV.addSubview(mainView)
        mainView.vstack(
            UIView(backgroundColor: .red).with(height: 500),
            UIView(backgroundColor: .yellow).with(height: 800)
        )
        
        NSLayoutConstraint.activate([
            mainView.widthAnchor.constraint(equalTo: self.scrollV.widthAnchor)
        ])
        
        mainView.anchor(
            .leading(scrollV.leadingAnchor, constant: 0),
            .top(scrollV.topAnchor, constant: 0),
            .bottom(scrollV.bottomAnchor, constant: 0)
        )
        
        scrollV.delegate = self
    }
    
    private func toDirection(curY: CGFloat) -> Direction {
        return lastY > curY ? .up : .down
    }
    
    private func updateAxis(direction: Direction) {
        y += direction == .up ? -1/3 : 1/3
        y = y < -88 ? -88 : y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        let direction = toDirection(curY: y)
        print(direction)
        updateAxis(direction: direction)
        print("ScrollV has y: \(y) and y = \(self.y)")
        
        lastY = y
    }
    
    enum Direction {
        case up, down
    }
}
