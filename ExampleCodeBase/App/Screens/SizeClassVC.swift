//
//  SizeClassVC.swift
//  ExampleCodeBase
//
//  Created by Lucas Pham on 2/27/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit

class SizeClassVC: UIViewController {

    var heighContraint : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .blue
        let spacer1 = UIView(backgroundColor: .clear)
        let spacer2 = UIView(backgroundColor: .clear)
        let mainV = UIView(backgroundColor: .red)
        
        heighContraint = mainV.heightAnchor.constraint(equalToConstant: 300)
        
        self.view.vstack(
            spacer1,
            mainV,
            spacer2
        )
        heighContraint.isActive = true
        
        spacer1.heightAnchor.constraint(equalTo: spacer2.heightAnchor).isActive = true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        if coordinator ==
        if UIDevice.current.orientation.isLandscape {
            heighContraint.constant = self.view.safeAreaLayoutGuide.layoutFrame.height
        } else {
            heighContraint.constant = 300
        }
    }
}
