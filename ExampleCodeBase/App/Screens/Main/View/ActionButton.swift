//
//  OptionButton.swift
//  ExampleCodeBase
//
//  Created by Lucas Pham on 2/9/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation
import UIKit

class ActionButton: UIButton {
    
    let type: ActionButtonType
    
    init(type: ActionButtonType) {
        self.type = type
        super.init(frame: .zero)
        self.setTitle(type.rawValue, for: .normal)
        self.setTitleColor(.systemBlue, for: .normal)
        self.setTitle("Tapped", for: .highlighted)
//        self.addTarget(target, action: selector, for: .touchUpInside)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

enum ActionButtonType: String {
    case push = "Push"
    case logIn = "Log in"
    case loadUser = "Load user"
    case insertUser = "Insert user"
    case deleteUser = "Delete user"
    case saveChange = "Save change to Core Data"
}
