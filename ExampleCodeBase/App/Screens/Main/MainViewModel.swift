//
//  MainViewModel.swift
//  BaseProject
//
//  Created by Lucas Pham on 2/4/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation
import RxSwift
import CoreDataHelper

protocol Main1ViewModeling {
    var simpleUser: PublishSubject<SimpleUser> { get }
    var listUser: PublishSubject<[SimpleUser]> { get }
    var errMessage: PublishSubject<String> { get }
    
    func logIn(username: String, pass: String)
    func loadUser()
    func addUser(_ name: String)
    func deleteUser(_ name: String)
    func saveChange()
}

protocol Main2ViewModeling {
    func logIn(username: String, pass: String)
}

class MainViewModel: BaseVM, Main1ViewModeling, Main2ViewModeling {
    
    //MARK: View Model
    var simpleUser: PublishSubject<SimpleUser> = .init()
    var errMessage: PublishSubject<String> = .init()
    var listUser: PublishSubject<[SimpleUser]> = .init()
    
    let userApi: UserApi
    let container: PersistentContainer
    
    init(userApi: UserApi, container: PersistentContainer) {
        self.userApi = userApi
        self.container = container
        
    }
    
    func logIn(username: String, pass: String) {
        userApi.logIn(username: username, password: pass)
            .subscribe(
                onNext: { [weak self] user in
                    let userDisplay = SimpleUser(
                        displayName: "Hello " + (user.id ?? "unknown"),
                        displayId: "Your id: " + (user.id ?? "unknown")
                    )
                    self?.simpleUser.onNext(userDisplay)
                },
                onError: { [weak self] err in
                    self?.errMessage.onNext(err.localizedDescription)
                }
            )
            .disposed(by: disposeBag)
    }
    
    func loadUser() {
        container.fetch(on: .background) { [weak self] (res: Result<[User], Error>) in
            switch res {
            case .success(let users):
//                users.map({ print($0.name) })
                self?.listUser.onNext(users.map({ SimpleUser(displayName: $0.name ?? "", displayId: "") }))
            case .failure(let err):
                self?.errMessage.onNext(err.localizedDescription)
            }
        }
    }
    
    func addUser(_ name: String) {
        container.insert(on: .background, setUpEntity: { (entity: User) in entity.name = name }) { err in
            guard let err = err else { return }
            self.errMessage.onNext(err.localizedDescription)
        }
    }
    func deleteUser(_ name: String) {
        container.delete(on: .background, whichInclude: { (user: User) -> Bool in
            user.name == name
        }) { err in
            guard let err = err else { return }
            self.errMessage.onNext(err.localizedDescription)
        }
    }
    func saveChange() {
        container.saveAllContexts()
    }
}

struct SimpleUser {
    var displayName: String
    var displayId: String
}
