//
//  ViewController.swift
//  BaseProject
//
//  Created by Lucas Pham on 1/10/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit
import DeclarativeUIKit

class MainVC: AppVC {
    
    //MARK: UI Element
    let nameTF = IndentedTextField(placeholder: "Name", padding: 50)
    let errLb = UILabel(text: "", textColor: .red)
    let listBtn: [ActionButton]
    
    let userTbV = UITableView()
    
    let viewModel: Main1ViewModeling
    let vcFactory: ViewControllerFactory
    let cellId = "CellId"
    var listUser: [SimpleUser] = []
    
    init(viewModel: Main1ViewModeling, vcFactory: ViewControllerFactory) {
        self.viewModel = viewModel
        self.vcFactory = vcFactory
        self.listBtn = [
            .init(type: .push),
            .init(type: .logIn),
            .init(type: .loadUser),
            .init(type: .insertUser),
            .init(type: .deleteUser),
            .init(type: .saveChange)
        ]
        super.init(nibName: nil, bundle: nil)
        self.listBtn.forEach({ $0.addTarget(self, action: #selector(tapActionBtn(_:)), for: .touchUpInside) })
    }
    
    //MARK: Do function
    override func setUp() {
        setBackground(with: .white)
        self.view.vstack(
            self.view.vstack(
                UILabel(text: "Work with other vc", textAlignment: .center),
                self.listBtn[0],
                UILabel(text: "Work with api", textAlignment: .center),
                self.listBtn[1],
                UILabel(text: "Work with core data", textAlignment: .center),
                UIView().hstack(
                    self.listBtn[2], self.listBtn[3], self.listBtn[4],
                    distribution: .fillEqually
                ),
                nameTF,
                self.listBtn[5],
                errLb,
                distribution: .fillEqually
            ).with(height: 300),
            userTbV
        )
        
        userTbV.dataSource = self
        userTbV.delegate = self
        userTbV.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    override func setUpRx() {
        viewModel.errMessage
            .subscribe(onNext: { msg in
                self.errLb.text = msg
            })
            .disposed(by: disposeBag)
        
        viewModel.simpleUser
            .subscribe(onNext: { simpleUser in
                print(simpleUser)
            })
            .disposed(by: disposeBag)
        
        viewModel.listUser
            .subscribe(onNext: { listUser in
                self.listUser = listUser
                self.userTbV.reloadData()
            })
            .disposed(by: disposeBag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Event function
    @objc func tapActionBtn(_ button: UIButton) {
        guard let button = button as? ActionButton else { return }
        errLb.text = ""
        switch button.type {
        case .push:
            push(vcFactory.makeSampleVC())
        case .logIn:
            viewModel.logIn(username: "abc", pass: "xyz")
        case .loadUser:
            viewModel.loadUser()
        case .insertUser:
            guard let text = nameTF.text, text.count > 0 else { return }
            viewModel.addUser(text)
        case .deleteUser:
            guard let text = nameTF.text, text.count > 0 else { return }
            viewModel.deleteUser(text)
        case .saveChange:
            viewModel.saveChange()
        default:
            break
        }
    }
}

extension MainVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UITableViewCell
        let user = listUser[indexPath.row]
        cell.textLabel?.text = user.displayName
        return cell
    }
}
