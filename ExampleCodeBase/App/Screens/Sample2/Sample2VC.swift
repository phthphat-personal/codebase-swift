//
//  Sample2VC.swift
//  BaseProject
//
//  Created by Lucas Pham on 1/11/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit

class Sample2VC: AppVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .systemBlue
    }

}
