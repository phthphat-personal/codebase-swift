//
//  SampleVC.swift
//  BaseProject
//
//  Created by Lucas Pham on 1/11/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit

class SampleVC: AppVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .systemRed
    }
}
