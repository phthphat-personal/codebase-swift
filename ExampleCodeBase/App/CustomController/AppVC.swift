//
//  AppVC.swift
//  BaseProject
//
//  Created by Lucas Pham on 2/4/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import UIKit
import RxSwift
import BaseUI
import DeclarativeUIKit

class AppVC: UIViewController, BaseVCHelper {
    
    public let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        setUpRx()
    }
    //MARK: Override to use
    open func setUp() {}
    open func setUpRx() {}
}
