//
//  ViewControllerFactory.swift
//  BaseProject
//
//  Created by Lucas Pham on 2/4/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation

protocol ViewControllerFactory {
    func makeMainVC() -> MainVC
    func makeSampleVC() -> SampleVC
    func makeTestScrollVC() -> TestScrollVC
}

extension DIContainer: ViewControllerFactory {
    
    func makeMainVC() -> MainVC {
        return MainVC(viewModel: self.makeMainViewModel(), vcFactory: self)
    }
    func makeSampleVC() -> SampleVC {
        return SampleVC()
    }
    
    func makeTestScrollVC() -> TestScrollVC {
        return TestScrollVC()
    }
}
