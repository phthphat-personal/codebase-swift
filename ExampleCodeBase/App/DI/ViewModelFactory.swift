//
//  ViewModelFactory.swift
//  BaseProject
//
//  Created by Lucas Pham on 2/5/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation

protocol ViewModelFactory {
    func makeMainViewModel() -> Main1ViewModeling
}

extension DIContainer: ViewModelFactory {
    func makeMainViewModel() -> Main1ViewModeling {
        return MainViewModel(userApi: self.makeUserApi(), container: self.coreDataContainer())
    }
}
