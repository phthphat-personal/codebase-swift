//
//  ApiFactory.swift
//  BaseProject
//
//  Created by Lucas Pham on 2/5/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation
import PNetworking

protocol ApiFactory {
    func makeNetworking() -> PNetworking
    
    func makeUserApi() -> UserApi
}

extension DIContainer: ApiFactory {
    func makeNetworking() -> PNetworking {
        return PNetwork(baseUrl: ApiUrl.baseUrl)
    }
    
    //MARK: Api
    func makeUserApi() -> UserApi {
        return UserApiService(networking: self.makeNetworking())
    }
}
