//
//  AppFactory.swift
//  BaseProject
//
//  Created by Lucas Pham on 2/4/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation
import UIKit
import CoreDataHelper

class DIContainer {
    func coreDataContainer() -> PersistentContainer {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    }
}
