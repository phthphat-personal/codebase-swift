//
//  Preview.swift
//  ExampleCodeBase
//
//  Created by Lucas Pham on 2/12/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import SwiftUI

struct Preview: View {
    var body: some View {
//        ScrollView{
            VStack {
                Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
                UIViewPreview()
            }
//        }
    }
}

struct Preview_Previews: PreviewProvider {
    static var previews: some View {
        Preview()
    }
}


struct UIViewPreview: UIViewRepresentable {
    
    func makeUIView(context: Context) -> UIView {
        return CustomUIView()
    }
    func updateUIView(_ view: UIView, context: Context) {
    }
}


class CustomUIView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    func setUpView() {
//        let view1 = UIView(backgroundColor: .red)
//        self.addSubview(view1.with(frame: .init(x: 0, y: 0, width: 200, height: 300)))
//        with(subView: UIView(backgroundColor: .red).with(frame: .init(x: 0, y: 0, width: 200, height: 300)))
//        view1.frame =
        vstack(
            UIButton(title: "Press me", titleColor: .systemPurple)
                .with(self, #selector(buttonTapped)),
            UIView()
                .with(backgroundColor: .red)
                .with(height: 300),
            UIView()
                .with(height: 200)
                .with(backgroundColor: .yellow)
                .vstack(UIView(backgroundColor: .systemBlue)).with(margins: .allSides(30)),
            hstack(
                UIView()
                    .with(backgroundColor: .blue)
                    .with(width: 110),
                UIView()
                    .with(backgroundColor: .green)
            )
        )
    }
    
    @objc func buttonTapped() {
        print("Yeah")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
