//
//  UserLoginedRes.swift
//  ExampleCodeBase
//
//  Created by Lucas Pham on 2/6/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation

class UserLogInResponse: Codable {
    var id: String?
    var username: String?
    var isBlocked: Bool?
}
