//
//  UserApiEndPoint.swift
//  ExampleCodeBase
//
//  Created by Lucas Pham on 2/6/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation
import PNetworking

enum UserApiEndPoint {
    case logIn(username: String, pass: String)
}

extension UserApiEndPoint: EndPoint {
    var path: String {
        return ""
    }
    
    var httpMethod: HttpMethod {
        return .post
    }
    
    var parameters: [String : Any] {
        return [:]
    }
    
    var headers: [String : String]? {
        return nil
    }
}
