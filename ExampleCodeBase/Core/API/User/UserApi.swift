//
//  UserApi.swift
//  BaseProject
//
//  Created by Lucas Pham on 2/4/20.
//  Copyright © 2020 phthphat. All rights reserved.
//

import Foundation
import NetworkLayer
import RxSwift
import PNetworking

protocol UserApi: APIService {
    func logIn(username: String, password: String) -> Observable<UserLogInResponse>
}

class UserApiService: UserApi {
    
    var networking: PNetworking
    
    required init(networking: PNetworking) {
        self.networking = networking
    }
    
    func logIn(username: String, password: String) -> Observable<UserLogInResponse> {
        let route = UserApiEndPoint.logIn(username: username, pass: password)
        return request(route)
    }
    
}
